let name = "Денис"
let n = 0
function plus() {
    n = n + 1;
    document.querySelector("#root").innerText = `${n}! = ${factorial(n)}`;
}
function minus() {
    n = n - 1;
    document.querySelector("#root").innerText = `${n}! = ${factorial(n)}`;
}
function factorial(n) {
    if (n < 0) return "Невозможно вычислить факториал";
    else if (n > 0) return n * factorial(n - 1);
    else return 1;
}
document.querySelector("#root").innerText = `${n}! = ${factorial(n)}`;